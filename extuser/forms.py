# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from extuser.models import ExtUser
from registration.forms import RegistrationForm

class ExtUserRegistrationForm(RegistrationForm):
    class Meta:
        model = ExtUser#get_user_model()
        fields = ('email',)

class ExtUserForm(forms.ModelForm):
    class Meta:
        model = ExtUser#get_user_model()
        fields = ('firstname', 'lastname')

class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(
        label='Password',
        widget=forms.PasswordInput
    )
    password2 = forms.CharField(
        label='Password confirm',
        widget=forms.PasswordInput
    )

    def clean_password2(self):
        # Check password1 and password2 to equal.
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Passwords is not equal!')
        return password2

    def save(self, commit=True):
        """Create new record in DB"""
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user

    class Meta:
        model = ExtUser#get_user_model()
        fields = ('email', )


class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField()

    def clean_password(self):
        return self.initial['password']

    def save(self, commit=True):
        user = super(UserChangeForm, self).save(commit=False)
        if commit:
            user.save()
        return user

    class Meta:
        model = ExtUser#get_user_model()
        fields = ['email', 'firstname', 'lastname', 'middlename']

    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField()

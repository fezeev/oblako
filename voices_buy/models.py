# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models
import datetime

# Create your models here.
class Voices_buy(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    pay_date = models.DateTimeField('Дата оплаты', auto_now_add=True, auto_now=False)
    voices = models.DecimalField('Количество оплаченных голосов', max_digits=2, decimal_places=0, null=False, blank=False, default=1)
    money = models.DecimalField('Денег потрачено на голоса', max_digits=17, decimal_places=2, null=False, blank=False, default=100)

    def get_pay_till(self):
        return datetime.datetime(self.pay_date.year, self.pay_date.month+1, self.pay_date.day, self.pay_date.hour, self.pay_date.minute, self.pay_date.second, self.pay_date.microsecond)

    def get_total_payd_money(self, user):
        result = 0
        for vb in self.Objects.filter(user=user):
            result += vb.money
        return result

# Rename this file to local_settings.py and fill it with correct product database values

import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

SECRET_KEY = '' # just unique and secret string
YANDEX_MONEY_SECRET_WORD = '' # take it from https://money.yandex.ru/myservices/online.xml

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# not so value information, but different from development stage ))
DEBUG = False
TEMPLATE_DEBUG = False

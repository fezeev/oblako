from django.conf.urls import patterns, include, url
from django.contrib import admin

from registration.backends.hmac.views import RegistrationView
from extuser.forms import ExtUserRegistrationForm


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'oblako.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/register/$',
        RegistrationView.as_view(
            form_class=ExtUserRegistrationForm
        ),
        name='registration_register',
    ),
    url(r'^accounts/', include('registration.backends.hmac.urls')),
    url(r'^accounts/profile/$', 'extuser.views.profile', name='profile'),
    url(r'^accounts/get_voice/$', 'extuser.views.get_voice', name='get_voice'),
    url(r'^$', 'elections.views.elections_list', name='index'),
    url(r'^elections/', include('elections.urls')),
    url(r'^yandex-money-notice$', 'yandexmoney_notice.views.http_notification'),
)
